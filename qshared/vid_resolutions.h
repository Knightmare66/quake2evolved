/*
Copyright (C) 1997-2001 Id Software, Inc.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.

*/

// List of video modes included in platform-specific video menus

	"Custom",
/*	"320 x 240",
	"400 x 300",
	"512 x 384",
	"640 x 480",
	"800 x 600",
	"960 x 720",
	"1024 x 768",
	"1152 x 864",
	"1280 x 1024",
	"1600 x 1200",
	"2048 x 1536",
	"856 x 480 (wide)",
	"1920 x 1200 (wide)"*/
	"320x240",
	"400x300",
	"512x384",
	"640x480",
	"800x600",
	"960x720",
	"1024x768",
	"1152x864",
	"1280x960",
	"1280x1024",
	"1400x1050",
	"1600x1200",
	"1920x1440",
	"2048x1536",
	"800x480",
	"856x480",
	"1024x600",
	"1280x720",
	"1280x768",
	"1280x800",
	"1360x768",
	"1366x768",
	"1440x900",
	"1600x900",
	"1600x1024",
	"1680x1050",	
	"1920x1080",
	"1920x1200",
	"2560x1080",
	"2560x1440",
	"2560x1600",
	"3200x1800",
	"3440x1440",
	"3840x1600",
	"3840x2160",
	"3840x2400",
	"4096x2160",
	"5120x2880"
