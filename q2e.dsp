# Microsoft Developer Studio Project File - Name="quake2" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=QUAKE2 - WIN32 RELEASE
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "q2e.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "q2e.mak" CFG="QUAKE2 - WIN32 RELEASE"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "quake2 - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "quake2 - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "quake2 - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir ".\Release"
# PROP BASE Intermediate_Dir ".\Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir ".\release"
# PROP Intermediate_Dir ".\release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /c
# ADD CPP /nologo /G5 /MT /W3 /GX /Zd /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "NDEBUG"
# ADD RSC /l 0x409 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib shell32.lib advapi32.lib winmm.lib wsock32.lib win32\lib\zlibstat.lib win32\lib\jpeg6b.lib win32\lib\ogg_static.lib win32\lib\vorbisfile_static.lib /nologo /subsystem:windows /machine:I386
# SUBTRACT LINK32 /profile /incremental:yes /debug /nodefaultlib

!ELSEIF  "$(CFG)" == "quake2 - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir ".\Debug"
# PROP BASE Intermediate_Dir ".\Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir ".\debug"
# PROP Intermediate_Dir ".\debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /c
# ADD CPP /nologo /G5 /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# SUBTRACT CPP /Fr
# ADD BASE MTL /nologo /D "_DEBUG" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x409 /d "_DEBUG"
# ADD RSC /l 0x409 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /debug /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib shell32.lib advapi32.lib winmm.lib wsock32.lib win32\lib\zlibstat.lib win32\lib\jpeg6b.lib win32\lib\ogg_static.lib win32\lib\vorbisfile_static.lib /nologo /subsystem:windows /incremental:no /map /debug /machine:I386
# SUBTRACT LINK32 /profile /nodefaultlib

!ENDIF 

# Begin Target

# Name "quake2 - Win32 Release"
# Name "quake2 - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;hpj;bat;for;f90"
# Begin Source File

SOURCE=.\win32\alw_win.c
# End Source File
# Begin Source File

SOURCE=.\win32\cd_win.c
# End Source File
# Begin Source File

SOURCE=.\client\cinematic.c
# End Source File
# Begin Source File

SOURCE=.\client\cl_demo.c
# End Source File
# Begin Source File

SOURCE=.\client\cl_draw.c
# End Source File
# Begin Source File

SOURCE=.\client\cl_effects.c
# End Source File
# Begin Source File

SOURCE=.\client\cl_ents.c
# End Source File
# Begin Source File

SOURCE=.\client\cl_input.c
# End Source File
# Begin Source File

SOURCE=.\client\cl_load.c
# End Source File
# Begin Source File

SOURCE=.\client\cl_localents.c
# End Source File
# Begin Source File

SOURCE=.\client\cl_main.c
# End Source File
# Begin Source File

SOURCE=.\client\cl_marks.c
# End Source File
# Begin Source File

SOURCE=.\client\cl_parse.c
# End Source File
# Begin Source File

SOURCE=.\client\cl_particles.c
# End Source File
# Begin Source File

SOURCE=.\client\cl_predict.c
# End Source File
# Begin Source File

SOURCE=.\client\cl_screen.c
# End Source File
# Begin Source File

SOURCE=.\client\cl_tempents.c
# End Source File
# Begin Source File

SOURCE=.\client\cl_view.c
# End Source File
# Begin Source File

SOURCE=.\qcommon\cmd.c
# End Source File
# Begin Source File

SOURCE=.\qcommon\cmodel.c
# End Source File
# Begin Source File

SOURCE=.\qcommon\common.c
# End Source File
# Begin Source File

SOURCE=.\client\console.c
# End Source File
# Begin Source File

SOURCE=.\qcommon\crc.c
# End Source File
# Begin Source File

SOURCE=.\qcommon\cvar.c
# End Source File
# Begin Source File

SOURCE=.\qcommon\filesystem.c
# End Source File
# Begin Source File

SOURCE=.\win32\glw_win.c
# End Source File
# Begin Source File

SOURCE=.\win32\in_win.c
# End Source File
# Begin Source File

SOURCE=.\client\keys.c
# End Source File
# Begin Source File

SOURCE=.\game\m_flash.c
# End Source File
# Begin Source File

SOURCE=.\qcommon\md4.c
# End Source File
# Begin Source File

SOURCE=.\qcommon\memory.c
# End Source File
# Begin Source File

SOURCE=.\qcommon\net_chan.c
# End Source File
# Begin Source File

SOURCE=.\qcommon\net_msg.c
# End Source File
# Begin Source File

SOURCE=.\win32\net_win.c
# End Source File
# Begin Source File

SOURCE=.\qcommon\pmove.c
# End Source File
# Begin Source File

SOURCE=.\qshared\q_math.c
# End Source File
# Begin Source File

SOURCE=.\qshared\q_shared.c
# End Source File
# Begin Source File

SOURCE=.\win32\qal_win.c
# End Source File
# Begin Source File

SOURCE=.\win32\qgl_win.c
# End Source File
# Begin Source File

SOURCE=.\refresh\r_alias.c
# End Source File
# Begin Source File

SOURCE=.\refresh\r_backend.c
# End Source File
# Begin Source File

SOURCE=.\refresh\r_draw.c
# End Source File
# Begin Source File

SOURCE=.\refresh\r_fragment.c
# End Source File
# Begin Source File

SOURCE=.\refresh\r_gl.c
# End Source File
# Begin Source File

SOURCE=.\refresh\r_light.c
# End Source File
# Begin Source File

SOURCE=.\refresh\r_main.c
# End Source File
# Begin Source File

SOURCE=.\refresh\r_model.c
# End Source File
# Begin Source File

SOURCE=.\refresh\r_program.c
# End Source File
# Begin Source File

SOURCE=.\refresh\r_shader.c
# End Source File
# Begin Source File

SOURCE=.\refresh\r_shadow.c
# End Source File
# Begin Source File

SOURCE=.\refresh\r_sky.c
# End Source File
# Begin Source File

SOURCE=.\refresh\r_surface.c
# End Source File
# Begin Source File

SOURCE=.\refresh\r_texture.c
# End Source File
# Begin Source File

SOURCE=.\client\s_dma.c
# End Source File
# Begin Source File

SOURCE=.\client\s_sfx.c
# End Source File
# Begin Source File

SOURCE=.\client\s_stream.c
# End Source File
# Begin Source File

SOURCE=.\server\sv_ccmds.c
# End Source File
# Begin Source File

SOURCE=.\server\sv_ents.c
# End Source File
# Begin Source File

SOURCE=.\server\sv_game.c
# End Source File
# Begin Source File

SOURCE=.\server\sv_init.c
# End Source File
# Begin Source File

SOURCE=.\server\sv_main.c
# End Source File
# Begin Source File

SOURCE=.\server\sv_send.c
# End Source File
# Begin Source File

SOURCE=.\server\sv_user.c
# End Source File
# Begin Source File

SOURCE=.\server\sv_world.c
# End Source File
# Begin Source File

SOURCE=.\win32\sys_win.c
# End Source File
# Begin Source File

SOURCE=.\ui\ui_advanced.c
# End Source File
# Begin Source File

SOURCE=.\ui\ui_audio.c
# End Source File
# Begin Source File

SOURCE=.\ui\ui_cinematics.c
# End Source File
# Begin Source File

SOURCE=.\ui\ui_controls.c
# End Source File
# Begin Source File

SOURCE=.\ui\ui_credits.c
# End Source File
# Begin Source File

SOURCE=.\ui\ui_defaults.c
# End Source File
# Begin Source File

SOURCE=.\ui\ui_demos.c
# End Source File
# Begin Source File

SOURCE=.\ui\ui_gameoptions.c
# End Source File
# Begin Source File

SOURCE=.\ui\ui_gotosite.c
# End Source File
# Begin Source File

SOURCE=.\ui\ui_ingame.c
# End Source File
# Begin Source File

SOURCE=.\ui\ui_loadgame.c
# End Source File
# Begin Source File

SOURCE=.\ui\ui_main.c
# End Source File
# Begin Source File

SOURCE=.\ui\ui_menu.c
# End Source File
# Begin Source File

SOURCE=.\ui\ui_mods.c
# End Source File
# Begin Source File

SOURCE=.\ui\ui_multiplayer.c
# End Source File
# Begin Source File

SOURCE=.\ui\ui_network.c
# End Source File
# Begin Source File

SOURCE=.\ui\ui_options.c
# End Source File
# Begin Source File

SOURCE=.\ui\ui_performance.c
# End Source File
# Begin Source File

SOURCE=.\ui\ui_playersetup.c
# End Source File
# Begin Source File

SOURCE=.\ui\ui_qmenu.c
# End Source File
# Begin Source File

SOURCE=.\ui\ui_quit.c
# End Source File
# Begin Source File

SOURCE=.\ui\ui_savegame.c
# End Source File
# Begin Source File

SOURCE=.\ui\ui_singleplayer.c
# End Source File
# Begin Source File

SOURCE=.\ui\ui_video.c
# End Source File
# Begin Source File

SOURCE=.\win32\vid_win.c
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl;fi;fd"
# Begin Source File

SOURCE=.\win32\alw_win.h
# End Source File
# Begin Source File

SOURCE=.\qshared\anorms.h
# End Source File
# Begin Source File

SOURCE=.\client\cdaudio.h
# End Source File
# Begin Source File

SOURCE=.\client\cinematic.h
# End Source File
# Begin Source File

SOURCE=.\client\client.h
# End Source File
# Begin Source File

SOURCE=.\client\console.h
# End Source File
# Begin Source File

SOURCE=.\game\game.h
# End Source File
# Begin Source File

SOURCE=.\refresh\glext.h
# End Source File
# Begin Source File

SOURCE=.\win32\glw_win.h
# End Source File
# Begin Source File

SOURCE=.\client\input.h
# End Source File
# Begin Source File

SOURCE=.\client\keys.h
# End Source File
# Begin Source File

SOURCE=.\refresh\normals.h
# End Source File
# Begin Source File

SOURCE=.\refresh\palette.h
# End Source File
# Begin Source File

SOURCE=.\qshared\q_shared.h
# End Source File
# Begin Source File

SOURCE=.\client\qal.h
# End Source File
# Begin Source File

SOURCE=.\qcommon\qcommon.h
# End Source File
# Begin Source File

SOURCE=.\qcommon\qfiles.h
# End Source File
# Begin Source File

SOURCE=.\refresh\qgl.h
# End Source File
# Begin Source File

SOURCE=.\refresh\r_local.h
# End Source File
# Begin Source File

SOURCE=.\client\refresh.h
# End Source File
# Begin Source File

SOURCE=.\client\s_local.h
# End Source File
# Begin Source File

SOURCE=.\server\server.h
# End Source File
# Begin Source File

SOURCE=.\client\sound.h
# End Source File
# Begin Source File

SOURCE=.\qshared\surfaceflags.h
# End Source File
# Begin Source File

SOURCE=.\client\ui.h
# End Source File
# Begin Source File

SOURCE=.\ui\ui_local.h
# End Source File
# Begin Source File

SOURCE=.\client\video.h
# End Source File
# Begin Source File

SOURCE=.\refresh\warpsin.h
# End Source File
# Begin Source File

SOURCE=.\win32\wglext.h
# End Source File
# Begin Source File

SOURCE=.\win32\winquake.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;cnt;rtf;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\win32\q2.ico
# End Source File
# Begin Source File

SOURCE=.\win32\q2.rc
# End Source File
# End Group
# End Target
# End Project
